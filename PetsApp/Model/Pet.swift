//
//  Pet.swift
//  PetsApp
//


import Foundation

struct Pet {
    var name: String
    var hasMajority: Bool
    var phone: String
    var race: String
    var gender: Gender
    
    enum Gender {
        case male, female
    }
    
    enum Status {
        case accepted
        case rejected(String)
    }
    
    var status: Status {
        if name == "" {
            return .rejected("Vous n'avez pas indiqué votre nom !")
        }
        if phone == "" {
            return .rejected("Vous n'avez pas indiqué votre téléphone !")
        }
        if race == "" {
            return .rejected("Quel est votre race ?")
        }
        if !hasMajority {
            return .rejected("Les mineurs ne sont pas admis.")
        }
        return .accepted
    }
}
