//
//  CatFormViewController.swift
//  PetsApp
//


import UIKit

class CatFormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var cat:Pet!
    var catRace = ["American curl", "Angora turc","Bobtail américain","British longhair","European shorthair","Munchkin","Ragdoll","Tiffany","Ojos azules","Chartreux"]
   
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var catsegment: UISegmentedControl!
    @IBOutlet weak var catSwitch: UISwitch!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var catPicker: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return catRace.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return catRace[row]
    }
 /////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func disapearKeyboard(_ sender: Any) {
        nameTextField.resignFirstResponder()
        phoneTextfield.resignFirstResponder()
    }
    
    /// Due to UITextFieldDelegate/////
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   /////////////
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "catsegue" {
            let catVC = segue.destination as! SuccessCatViewController
            catVC.cat = cat
        }
    }
    
    
    @IBAction func validateAction(_ sender: Any) {
        let gender: Pet.Gender = (catsegment.selectedSegmentIndex == 0) ? .male : .female
        let race = catRace[catPicker.selectedRow(inComponent: 0)]
        cat = Pet(name: nameTextField.text!, hasMajority: catSwitch.isOn, phone: phoneTextfield.text!, race: race, gender: gender)
        performSegue(withIdentifier: "catsegue", sender: (Any).self)
        print(cat)
    }
    
}
