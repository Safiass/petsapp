//
//  SuccessCatViewController.swift
//  PetsApp
//


import UIKit

class SuccessCatViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    var cat: Pet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        writeOnLabel()
 }

    func writeOnLabel() {
        messageLabel.text = "Hey the Cutest Animal in the World \(cat.name) "
    }
    
}
