//
//  WelcomeCatViewController.swift
//  PetsApp
//

import UIKit

class WelcomeDogViewController: UIViewController {

    @IBOutlet weak var dogImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        animateImage(image: dogImage, minusY: -50, plusY: 50)
    }
    
    public func animateImage(image:UIImageView, minusY: CGFloat, plusY: CGFloat) {
        UIView.animate(withDuration: 1, animations: {
            image.frame.origin.y -=  minusY
        }) { _ in
            UIView.animateKeyframes(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat], animations: {
                image.frame.origin.y += plusY
            })
        }
    }

}
