//
//  FormulaireViewController.swift
//  PetsApp
//

import UIKit

class FormulaireViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var myPet: Pet!
    let raceList = ["Afghan Hound", "Basset Hound", "Chihuahua", "Dalmatian", "Golden Retriever", "Siberian Husky", "Samoyed", "Welsh Corgi", "Boston terrier", "Poodle"]

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var myPicker: UIPickerView!
 
////////////////////// ViewDidLoad ///////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
}
///////////////////////////////////////////////////////////////////////////////////////////

/////////// Conform to UIPickerViewProtocoles ////////////////////////////////////////////
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return raceList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return raceList[row]
    }
///////////////////////////////////////////////////////////////////////////////////////////
    
///////////////to dismiss the keyBoard when finishing ////////////////////////////////////
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
        phoneField.resignFirstResponder()
    }
///////////////////////////////////////////////////////////////////////////////////////////
    
//////////////// continue when finishing and the keyboard disapears //////////////////////
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
 //////////////////////////////////////////////////////////////////////////////////////////
    
/////////////////////Button Validate//////////////////////////////////////////////////////
    // Recupérer l'objet Pet et le transferer eu controlleur suivant
    @IBAction func validateAction(_ sender: Any) {
       let gender: Pet.Gender = (segment.selectedSegmentIndex == 0) ? .male : .female
       let race = raceList[myPicker.selectedRow(inComponent: 0)]
       myPet = Pet(name: textField.text!, hasMajority: mySwitch.isOn, phone: phoneField.text!, race: race, gender: gender)
        checkState()
}
//////////////////////////////////////////////////////////////////////////////////////////
    
////////////////////Passage à une autre view ////////////////////////////////////////////
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSuccess" {
            let successVC = segue.destination as! SuccessViewController
            successVC.dog = myPet // On passe la donnée via les propriétés
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////
    
    func checkState(){
        switch myPet.status {
        case .accepted:
            performSegue(withIdentifier:"segueSuccess", sender: Any?.self)
        case .rejected(let error):
            let alertVC = UIAlertController(title: "Erreur", message: "Message d'erreur", preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertVC.addAction(action)
            present(alertVC, animated: true, completion: nil)
        }
        
    }
}
