//
//  SuccessViewController.swift
//  PetsApp
//


import UIKit

class SuccessViewController: UIViewController {

    var dog: Pet!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showMessage()
}

    func showMessage() {
        messageLabel.text = "Welcome Cute Dog \(dog.name). We wish that you find new friend."
    }
    
    
}

